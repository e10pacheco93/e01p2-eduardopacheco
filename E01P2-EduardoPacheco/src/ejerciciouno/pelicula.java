/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author root
 */
public class pelicula {

    private Object [] category;
    private String title;
    private int  duration;

    public pelicula(){
        
    }
    
    public pelicula(Object[] category, String title, int duration) {
        this.category = new Object[]{"Suspenso", "Infantil", "Terror", "Accion", "Romance"};
        this.title = title;
        this.duration = duration;
    }

    public Object[] getCategory() {
        return category;
    }

    public void setCategory(Object[] category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
    
   


    @Override
    public String toString() {
        return "pelicula{" + "category=" + category + ", title=" + title + ", duration=" + duration + '}';
    }
    
    
}
